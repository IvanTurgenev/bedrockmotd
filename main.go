package main

import (
	"fmt"
	"io/ioutil"
	"github.com/sandertv/gophertunnel/minecraft"
)

func main() {
	data, err := ioutil.ReadFile("motd.txt")
    if err != nil {
        fmt.Println("File reading error", err)
        return
    }
	listener, err := minecraft.Listen("raknet", "0.0.0.0:19132")
	if err != nil {
		panic(err)
	}
	// _ = listener.HijackPong("mco.mineplex.com:19132")
	listener.ServerName = string(data)

	for {
		conn, err := listener.Accept()
		if err != nil {
			return
		}
		go func() {
			// Process the connection on another goroutine as you would with TCP connections.
			defer conn.Close()
			for {
				// Read a packet from the client.
				if _, err := conn.(*minecraft.Conn).ReadPacket(); err != nil {
					return
				}
			}
		}()
	}
}