# builder image
FROM golang:alpine as builder
RUN mkdir /build
ADD *.go /build/
WORKDIR /build
ENV GO111MODULE=on
RUN go mod init gitlab.com/IvanTurgenev/bedrockmotd; go list -m all
RUN CGO_ENABLED=0 GOOS=linux go build -a -o main .


# generate clean, final image for end users
FROM alpine:latest
RUN mkdir /app
WORKDIR /app
COPY --from=builder /build/main .
ADD motd.txt .
# executable
ENTRYPOINT [ "./main" ]
